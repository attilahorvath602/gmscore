/*
 * Copyright (C) 2013-2017 microG Project Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.microg.gms.control;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.legacy.content.WakefulBroadcastReceiver;

import org.microg.gms.checkin.ActivateCheckinControl;
import org.microg.gms.checkin.ActivateGcmControl;
import org.microg.gms.droidguard.core.DroidGuardPreferences;
import org.microg.gms.safetynet.SafetyNetPreferences;

public class ControlReceiver extends WakefulBroadcastReceiver {
    private static final String TAG = "GmsControl";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            if ("com.google.android.gms.control.ENABLE_GCM_SAFETYNET".equals(intent.getAction())) {
                ActivateCheckinControl.run(context, true);
                ActivateGcmControl.run(context, true);
                SafetyNetPreferences.setEnabled(context, true);
                DroidGuardPreferences.setEnabled(context, true);
            }
            // if ("com.google.android.gms.control.DISABLE_GCM".equals(intent.getAction())) {
            //     ActivateGcmControl.run(context, false);
            // }
        } catch (Exception e) {
            Log.w(TAG, e);
        }
    }
}
